### NFC　讀取模式結合對稱式加密 (Android) ###

版本 1.0

* ReadingTag
 *  1. 可讀出Tag 的類型與最大儲存容量(Bytes)。
 *  2. 可讀出Tag 的儲存內容(TNF_WELL_KNOWN with RTD_TEXT)。
 *  3. 讀出的密文(cipher text) 可經由解密後產生明文(plain text)。

* Writing Tag
 * 1. 將文字訊息經過加密後建立TNF_WELL_KNOWN with RTD_TEXT 型態的Record。
 * 2. 將建立的Record 包裝為NDEF Message 然後寫入Tag。(請注意Tag 儲存空間有限，無法儲存過長的文字訊息)