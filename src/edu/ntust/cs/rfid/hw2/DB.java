package edu.ntust.cs.rfid.hw2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class DB {
	private Context mCtx = null;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;

	public DB(Context ctx) {
		this.mCtx = ctx;
	}

	public DB open() throws SQLException {
		dbHelper = new DatabaseHelper(mCtx);
		db = dbHelper.getWritableDatabase();

		return this;
	}

	public void close() {
		dbHelper.close();
	}

	public void beginTransaction() {
		db.beginTransaction();
	}

	public void transactionSuccessful() {
		db.setTransactionSuccessful();
	}

	public void endTransaction() {
		db.endTransaction();
	}

	// �s����_
	public static final String TABLE_KEY_DATA = "key_data";
	public static final String KEY_KEY = "key";

	// insert sqlite
	public long insertKey_data(String key) {
		ContentValues args = new ContentValues();
		args.put(KEY_KEY, key);

		return db.insert(TABLE_KEY_DATA, null, args);
	}

	// getData
	public Cursor getKey_data() {
		return db.rawQuery("select * from key_data", null);
	}
	// deleteData
	public int deleteKey_data() {
		return db.delete(TABLE_KEY_DATA, null, null);
	}

	// removeData

	public boolean removeKey_data(String key) {
		return db.delete(TABLE_KEY_DATA, KEY_KEY + "=" + key, null) > 0;
	}

	// CREATE table
	private static final String DATABASE_NAME = "nfc.db";
	private static final int DATABASE_VERSION = 2;
	private static final String CREATE_KEY_DATA = "CREATE TABLE IF NOT EXISTS key_data("
			+ "key varchar primary key not null" + ");";

	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_KEY_DATA);
			System.out.println("onCreate DB");

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			onCreate(db);
		}
	}

}
