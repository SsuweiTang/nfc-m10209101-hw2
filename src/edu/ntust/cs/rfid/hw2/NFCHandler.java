package edu.ntust.cs.rfid.hw2;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public interface NFCHandler {
	
public  boolean isSupported();
	
	public boolean isEnable();
	
	public void disableForeground();
	
	public void enableForegrount();


}
