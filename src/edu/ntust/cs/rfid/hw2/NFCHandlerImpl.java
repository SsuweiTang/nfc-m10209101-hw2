package edu.ntust.cs.rfid.hw2;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class NFCHandlerImpl implements NFCHandler {
		private Activity activity;
	private NfcAdapter nfcAdapter;
	private PendingIntent intent;
	private IntentFilter[] filters;
	
	public NFCHandlerImpl(Activity activity)
	{
		this.activity=activity;
		//檢查裝置是否有支援NFC
		nfcAdapter=NfcAdapter.getDefaultAdapter(activity.getApplicationContext());	
//		System.out.println("Hello nfcAdapter=="+nfcAdapter); 
		intent=PendingIntent.getActivity(activity, 0, new Intent(activity,activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected =new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		filters=new IntentFilter[]{tagDetected};	
	}

	@Override
	public boolean isSupported() {
		return nfcAdapter!=null;
	}

	@Override
	public boolean isEnable() {
		return nfcAdapter.isEnabled();
	}
	@Override
	public void disableForeground() {
		nfcAdapter.disableForegroundDispatch(activity);
	}

	@Override
	public void enableForegrount() {
		nfcAdapter.enableForegroundDispatch(activity, intent, filters, null);
	}
}

