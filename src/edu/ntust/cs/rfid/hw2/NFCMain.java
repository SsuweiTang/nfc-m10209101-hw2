package edu.ntust.cs.rfid.hw2;

/**
 * 
 * @author Suc-Wei Tang
 *
 */

import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.primitives.Bytes;
import edu.ntust.cs.rfid.hw2.lib.AES;
import edu.ntust.cs.rfid.hw2.lib.DES;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class NFCMain extends Activity {
	private DB mDbHelper = new DB(NFCMain.this);
	private NFCHandler nfcHandler;
	private TextView tv_type;
	private TextView tv_size;
	private TextView tv_content;
	private Button btn_scanTag;
	IntentFilter[] gWriteTagFilters;
	private boolean tagCall = false;
	private String NDEFType;
	private int NDEFSize = 0;
	private String MsgType;
	private String Payload;
	private String MsgId;
	private int MsgSize = 0;
	private short MsgTnf = 0;
	private String MessageContent;
	private AlertDialog alertDialog;
	private final static String CALL = "android.intent.action.CALL";
	private Button btn_tab_read;
	private Button btn_tab_write;
	private Button btn_generate_key;
	private Button btn_encrypt;
	private LinearLayout li_read;
	private LinearLayout li_write;
	private boolean tag_write = false;
	private boolean tag_scan = false;
	private boolean tag_onResume = false;
	private String NdefTNF = "";
	private String writeResult = "";
	private byte[] key2;
	private String cipherData;
	private TextView write_tv_cipher_text;
	private EditText write_edt_plain_text;
	private TextView read_tv_plain_text;
	private TextView read_tv_cipher_text;
	private Button btn_decrypt;
	private byte[] inputData;
	private Button btn_menu_key;
	private PopupWindow popupWindow;
	private Button pop_btn_generate_key;
	private TextView pop_tv_key ;
	private Button pop_btn_exit ;
	private String txtRndcoding = "";
	private int screenWidth;
	private int screenHeight;
	private int dialgoWidth;
	private int dialgoheight;
	String sqliteKey = "";

	private static final BiMap<Byte, String> URI_PREFIX_MAP = ImmutableBiMap
			.<Byte, String> builder().put((byte) 0x00, "")
			.put((byte) 0x01, "http://www.").put((byte) 0x02, "https://www.")
			.put((byte) 0x03, "http://").put((byte) 0x04, "https://")
			.put((byte) 0x05, "tel:").put((byte) 0x06, "mailto:")
			.put((byte) 0x07, "ftp://anonymous:anonymous@")
			.put((byte) 0x08, "ftp://ftp.").put((byte) 0x09, "ftps://")
			.put((byte) 0x0A, "sftp://").put((byte) 0x0B, "smb://")
			.put((byte) 0x0C, "nfs://").put((byte) 0x0D, "ftp://")
			.put((byte) 0x0E, "dav://").put((byte) 0x0F, "news:")
			.put((byte) 0x10, "telnet://").put((byte) 0x11, "imap:")
			.put((byte) 0x12, "rtsp://").put((byte) 0x13, "urn:")
			.put((byte) 0x14, "pop:").put((byte) 0x15, "sip:")
			.put((byte) 0x16, "sips:").put((byte) 0x17, "tftp:")
			.put((byte) 0x18, "btspp://").put((byte) 0x19, "btl2cap://")
			.put((byte) 0x1A, "btgoep://").put((byte) 0x1B, "tcpobex://")
			.put((byte) 0x1C, "irdaobex://").put((byte) 0x1D, "file://")
			.put((byte) 0x1E, "urn:epc:id:").put((byte) 0x1F, "urn:epc:tag:")
			.put((byte) 0x20, "urn:epc:pat:").put((byte) 0x21, "urn:epc:raw:")
			.put((byte) 0x22, "urn:epc:").put((byte) 0x23, "urn:nfc:").build();

	private TextView tv_result;
	private String data = "";
	private NdefMessage NDEFMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_main);
		nfcHandler = new NFCHandlerImpl(this);
		findView();
		mDbHelper.open();

		sqliteKey = getKeyFromSqlite();
		key2 = Base64.decode(sqliteKey, Base64.DEFAULT);	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		btn_menu_key = (Button) menu.findItem(R.id.key_generatekey)
				.getActionView().findViewById(R.id.menu_key_btn_key);
		btn_menu_key.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 不偏移
				getPopupWindow();
				popupWindow.showAsDropDown(v);
				// Toast.makeText(NFCMain.this,v.getId()+"出現出現",
				// Toast.LENGTH_LONG).show();
			}
		});
		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * 取得PopupWindow實例
	 */
	private void getPopupWindow() {

		if (null != popupWindow) {
			popupWindow.dismiss();
			return;
		} else {
			initPopuptWindow();
		}
	}

	protected void initPopuptWindow() {
		View popupWindow_view = getLayoutInflater().inflate(
				R.layout.generatekey, null, false);
		screenWidth = this.getWindowManager().getDefaultDisplay().getWidth();
		screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
		dialgoWidth = screenWidth / 5 * 4;
		dialgoheight = screenHeight / 2;

		popupWindow = new PopupWindow(popupWindow_view, dialgoWidth,dialgoheight, true);
		pop_btn_generate_key = (Button) popupWindow_view.findViewById(R.id.generatekey_btn_generatekey);
		pop_tv_key = (TextView) popupWindow_view.findViewById(R.id.generatekey_tv_key);
		pop_tv_key.setText(sqliteKey);

		System.out.println("sqlite key to byte[]=="	+ Base64.decode(sqliteKey, Base64.DEFAULT));

		pop_btn_exit = (Button) popupWindow_view.findViewById(R.id.generatekey_btn_exit);

		pop_btn_generate_key.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				try {
					// key2 = AES.generateKey();
					key2 = DES.generateKey();
					// System.out.println(Base64.encodeBase64String(key2));
					System.out.println("KEY=" + key2);
					System.out.println("KEY="+ Base64.encodeToString(key2, Base64.DEFAULT));
					insertKeyToSqlite(Base64.encodeToString(key2,Base64.DEFAULT));
				} catch (Exception e){
					e.printStackTrace();
				}
				pop_tv_key.setText(Base64.encodeToString(key2, Base64.DEFAULT));
			}
		});

		pop_btn_exit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				popupWindow.dismiss();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("NFCRead=============Resume============="
				+ getIntent().getAction() + tagCall);
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
			Tag detectTag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
			if (tag_write == true) {
				System.out.println("==============write=================");
				// NDEFMsg = getNdefMsg_from_RTD_URI(data, (byte) 0x05, false);
				// true 為 utf-8編碼
				NDEFMsg = getNdefMsg_from_RTD_TEXT(data, true, false);
				System.out.println("=============" + data + "=============");
				writeTag(NDEFMsg, detectTag);
				tv_result.setText(writeResult);
			} else {
				parserIntent(getIntent());
				// setIntent(new Intent());
			}

		}
		if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())) {
			// Tag detectTag =
			// getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		}
		nfcHandler.enableForegrount();
	}

	@Override
	protected void onPause() {
		super.onPause();
		tagCall = false;
		nfcHandler.disableForeground();
		if (alertDialog != null) {
			alertDialog.cancel();
			// tag_scan=false;
		}
	}

	@Override
	public void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

	private void findView() {
		tv_size = (TextView) findViewById(R.id.nfc_main_read_tv_size);
		tv_type = (TextView) findViewById(R.id.nfc_main_read_tv_type);
		tv_content = (TextView) findViewById(R.id.nfc_main_read_tv_content);
		btn_scanTag = (Button) findViewById(R.id.nfc_main_btn_scanTag);
		btn_tab_read = (Button) findViewById(R.id.tab_btn_nfc_read);
		btn_tab_write = (Button) findViewById(R.id.tab_btn_nfc_write);
		li_read = (LinearLayout) findViewById(R.id.nfc_main_li_read);
		li_write = (LinearLayout) findViewById(R.id.nfc_main_li_write);
		tv_result = (TextView) findViewById(R.id.nfc_main_write_tv_result);
		// edt_tel = (EditText) findViewById(R.id.nfc_main_write_edt_tel);
		write_edt_plain_text = (EditText) findViewById(R.id.nfc_main_write_edt_plain_text);

		btn_generate_key = (Button) findViewById(R.id.nfc_main_btn_generate_key);
		btn_encrypt = (Button) findViewById(R.id.nfc_main_write_btn_encrypt);
		btn_decrypt = (Button) findViewById(R.id.nfc_main_read_btn_decrypt);

		write_tv_cipher_text = (TextView) findViewById(R.id.nfc_main_write_tv_cipher_text);
		read_tv_plain_text = (TextView) findViewById(R.id.nfc_main_read_tv_plain_text);
		read_tv_cipher_text = (TextView) findViewById(R.id.nfc_main_read_tv_cipher_text);

		btn_tab_read.setOnClickListener(tabListener);
		btn_tab_write.setOnClickListener(tabListener);

		btn_generate_key.setOnClickListener(GenerateKeyListener);
		btn_encrypt.setOnClickListener(EncryptListener);
		btn_decrypt.setOnClickListener(DecryptListener);

		btn_tab_read.setTag("tab_read");
		btn_tab_write.setTag("tab_write");
		btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray2));
		btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray));

		btn_scanTag.setOnClickListener(sanTagListener);

		if (!nfcHandler.isSupported()) {
			// tv_result.setText(R.string.nfc_unsupported);
			return;
		}

		if (!nfcHandler.isEnable()) {
			// tv_result.setText(R.string.nfc_disable);
		}
		// tv_result.setText(R.string.nfc_ready);
	}

	@Override
	public void onNewIntent(Intent intent) {

		if (tag_scan == true) {
			tag_scan = false;
			Vibrator myVibrator = (Vibrator) getApplication().getSystemService(	Service.VIBRATOR_SERVICE);
			myVibrator.vibrate(500);
			setIntent(intent);

		}
		tagCall = true;

		System.out.println(tag_scan + "====onNewIntent==" + intent.getAction());
	}

	private void parserIntent(Intent intent) {
		// TODO Auto-generated method stub

		System.out.println("==============ParserIntent===================");
		NdefMessage[] msgs = null;
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		Ndef getNdef = Ndef.get(tag);
		NDEFType = getNdefType(getNdef.getType());
		NDEFSize = getNdef.getMaxSize();
		tv_type.setText(NDEFType);
		tv_size.setText(NDEFSize + "Byte");
		Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

		if (rawMsgs != null) {
			msgs = new NdefMessage[rawMsgs.length];
			for (int i = 0; i < rawMsgs.length; i++) {
				msgs[i] = (NdefMessage) rawMsgs[i];
				MsgSize += msgs[i].toByteArray().length;
				System.out.println("msg[" + i + "]" + msgs[i]);
			}
		} else {
			// Unknown tag type
			byte[] empty = new byte[] {};
			NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty,empty, empty);
			NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
			msgs = new NdefMessage[] { msg };
		}
		processMsg(msgs);
	}

	private String getNdefType(String type) {
		String myNdefType = "";
		if (type.toString().equals("com.nxp.ndef.mifareclassic")) {
			myNdefType = "NDEF on MIFARE Classic";
		} else if (type.toString().equals("org.nfcforum.ndef.type1")) {
			myNdefType = "NFC Forum Tag Type 1";
		} else if (type.toString().equals("org.nfcforum.ndef.type2")) {
			myNdefType = "NFC Forum Tag Type 2";
		} else if (type.toString().equals("org.nfcforum.ndef.type3")) {
			myNdefType = "NFC Forum Tag Type 3";
		} else if (type.toString().equals("org.nfcforum.ndef.type4")) {
			myNdefType = "NFC Forum Tag Type 4";
		}
		return myNdefType;
	}

	private void processMsg(NdefMessage[] msgs) {
		if (msgs == null || msgs.length == 0) {
			System.out.println("NdefMag==null");
		} else {
			for (int i = 0; i < msgs.length; i++) {
				int msglen = msgs[i].getRecords().length;
				NdefRecord[] records = msgs[i].getRecords();
				for (int j = 0; j < msglen; j++) // 幾個記錄
				{
					for (NdefRecord record : records) {
						if (record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							if (Arrays.equals(record.getType(),	NdefRecord.RTD_URI)) {
								Payload = parserWellKnownRtdUriPayload(record);
							} else if (Arrays.equals(record.getType(),NdefRecord.RTD_TEXT)) {
								Payload = WellKnownRtdTextPayload(record);
							}
						}
						if (record.getTnf() == NdefRecord.TNF_ABSOLUTE_URI) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_MIME_MEDIA) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE) {
							Payload = parserABSOLUTE_URIPayload(record);
						}
						NdefTNF = getNDEF(record.getTnf());
						MsgType = getRecordType(new String(record.getType()));
						MsgId = new String(record.getId());
						MessageContent = NdefTNF + "   " + MsgType + "\n"+ "Encoding" + "   " + txtRndcoding;
						tv_content.setText(MessageContent);
						read_tv_cipher_text.setText(Payload);
						tag_scan = false;
						if (tagCall == true	&& record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
							call(Payload);
						}
					}
				}
			}
		}

	}

	private String WellKnownRtdTextPayload(NdefRecord record) {
		String myPayload = "";
		Preconditions.checkArgument(Arrays.equals(record.getType(),NdefRecord.RTD_TEXT));
		byte[] payload = record.getPayload();
		Byte FirstByte = record.getPayload()[0];
		// bit 7 看編碼
		txtRndcoding = ((FirstByte & 0200) == 0) ? "UTF-8" : "UTF-16";
//		mylocale
		int langCodeLen = FirstByte & 0077;
		String langCode = new String(payload, 1, langCodeLen,Charset.forName("UTF-8"));
		try {
			myPayload = new String(payload, langCodeLen + 1, payload.length
					- langCodeLen - 1, txtRndcoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("txtRndcoding==" + txtRndcoding + "  "+ "langCodeLen  " + langCodeLen);
		return myPayload;
	}

	private String parserABSOLUTE_URIPayload(NdefRecord record) {
		byte[] payload = record.getPayload();
		Uri uri = Uri.parse(new String(payload, Charset.forName("UTF-8")));
		return uri.toString();
	}

	private String getRecordType(String type) {
		String myRecordType = "";
		if (type.equals("T"))
			myRecordType = "TEXT";
		else if (type.equals("U"))
			myRecordType = "URI";
		else if (type.equals("Sp"))
			myRecordType = "SMART_POSTER";
		else if (type.equals("ac"))
			myRecordType = "ALTERNATIVE_CARRIER";
		else if (type.equals("Hc"))
			myRecordType = "HANDOVER_CARRIER";
		else if (type.equals("Hr"))
			myRecordType = "HANDOVER_REQUEST";
		else if (type.equals("Hs"))
			myRecordType = "HANDOVER_SELECT";
		else {
			myRecordType = "";
		}
		return myRecordType;
	}

	private String getNDEF(short tnf) {
		String myNdefTNF = "";
		if (tnf == NdefRecord.TNF_EMPTY) // 000
		{
			myNdefTNF = "TNF_EMPTY";
		}
		if (tnf == NdefRecord.TNF_WELL_KNOWN) // 001
		{
			myNdefTNF = "TNF_WELL_KNOWN";
		}
		if (tnf == NdefRecord.TNF_MIME_MEDIA)// 002
		{
			myNdefTNF = "TNF_MIME_MEDIA";
		}
		if (tnf == NdefRecord.TNF_ABSOLUTE_URI)// 003
		{
			myNdefTNF = "TNF_ABSOLUTE_URI";
		}
		if (tnf == NdefRecord.TNF_EXTERNAL_TYPE) // 004
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNKNOWN) // 005
		{
			myNdefTNF = "TNF_UNKNOWN";
		}
		if (tnf == NdefRecord.TNF_UNCHANGED) // 006
		{
			myNdefTNF = "TNF_UNCHANGED";
		}
		return myNdefTNF;
	}

	private void call(String myPayload) {
		if (myPayload.startsWith("tel:", 0)) {
			Intent intentcall = new Intent(CALL, Uri.parse(myPayload));
			startActivity(intentcall);
			System.out.println("======call=======" + tagCall);
		}
	}
	private String parserWellKnownRtdUriPayload(NdefRecord record) {
		Preconditions.checkArgument(Arrays.equals(record.getType(),	NdefRecord.RTD_URI));
		byte[] payload = record.getPayload();
		String prefix = URI_PREFIX_MAP.get(payload[0]);
		byte[] fullUri = Bytes.concat(prefix.getBytes(Charset.forName("UTF-8")),Arrays.copyOfRange(payload, 1, payload.length));
		Uri uri = Uri.parse(new String(fullUri, Charset.forName("UTF-8")));
		return uri.toString();
	}

	private Button.OnClickListener sanTagListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			nfcHandler.enableForegrount();
			tag_scan = true;
			if (tag_write == true) {
				if (!write_tv_cipher_text.getText().toString().equals("")) {
					data = write_tv_cipher_text.getText().toString().trim();
				} else {
					Toast.makeText(NFCMain.this, "telephone is null",Toast.LENGTH_SHORT).show();
				}
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(NFCMain.this);
			builder.setTitle("Approach a NFC Tag");
			// builder.setMessage("Approach a NFC Tag");
			builder.setIcon(getResources().getDrawable(R.drawable.dialogicon));
			builder.setPositiveButton("Cancel",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							nfcHandler.disableForeground();
							alertDialog.cancel();
						}
					});

			if (write_edt_plain_text.getText().toString().equals("")&& tag_write == true) {
			} else {
				alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(false);
				alertDialog.show();
			}

		}
	};

	private Button.OnClickListener EncryptListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// edt_tel.getText().toString().equals("")
			if (write_edt_plain_text.getText().toString().equals("")) {
				Toast.makeText(NFCMain.this, "text is null", Toast.LENGTH_SHORT).show();
			} else {
				inputData = write_edt_plain_text.getText().toString().getBytes();
				try {
					// inputData = AES.encrypt(inputData, key2);
					inputData = DES.encrypt(inputData, key2);
					System.out.println("After encryption: "	+ Base64.encodeToString(inputData, Base64.DEFAULT));
					write_tv_cipher_text.setText(Base64.encodeToString(	inputData, Base64.DEFAULT));
					cipherData = Base64.encodeToString(inputData,Base64.DEFAULT);
					// read_tv_cipher_text.setText(cipherData);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	private Button.OnClickListener DecryptListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// edt_tel.getText().toString().equals("")
			byte[] output = null;
			byte[] input = null;

			String text = null;
			if (read_tv_cipher_text.getText().toString().equals("")) {
				Toast.makeText(NFCMain.this, "cipher text is null",	Toast.LENGTH_SHORT).show();
			} else {
				text = read_tv_cipher_text.getText().toString();
				System.out.println("cipher text=="+ read_tv_cipher_text.getText().toString());
				System.out.println("read outputdata bytes==" + input);
				input = Base64.decode(text, Base64.DEFAULT);
				System.out.println("input===" + input);
				try {

					if (input != null) {
						// output = AES.decrypt(input, key2);
						output = DES.decrypt(input, key2);
					}
					else {
						System.out.println("not null");
					}
					String outputString = new String(output);
					System.out.println("After decryption: " + outputString);
					read_tv_plain_text.setText(outputString);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
	private Button.OnClickListener GenerateKeyListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				// key2 = AES.generateKey();
				key2 = DES.generateKey();
				// System.out.println(Base64.encodeBase64String(key2));
				System.out.println("KEY="+ Base64.encodeToString(key2, Base64.DEFAULT));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};

	private Button.OnClickListener tabListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getTag().toString().equals("tab_read")) {
				tag_write = false;
				li_read.setVisibility(View.VISIBLE);
				li_write.setVisibility(View.GONE);
				btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray2));
				btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray));

			}
			if (v.getTag().toString().equals("tab_write")) {
				tag_write = true;
				li_read.setVisibility(View.GONE);
				li_write.setVisibility(View.VISIBLE);
				btn_tab_write.setBackgroundColor(getResources().getColor(R.color.gray2));
				btn_tab_read.setBackgroundColor(getResources().getColor(R.color.gray));
				// edt_tel.setText("");
			}

		}
	};

	// 建立TNF_WELL_KNOWN with RTD_URI (tel)
	public static NdefMessage getNdefMsg_from_RTD_URI(String uriFiledStr,
			byte identifierCode, boolean flagAddAAR) {
		byte[] uriField = uriFiledStr.getBytes(Charset.forName("US-ASCII"));
		byte[] payLoad = new byte[uriField.length + 1];
		payLoad[0] = identifierCode;
		System.arraycopy(uriField, 0, payLoad, 1, uriField.length);
		NdefRecord rtdUriRecord1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_URI, new byte[0], payLoad);
		return new NdefMessage(new NdefRecord[] { rtdUriRecord1 });
	}

	// 建立TNF_WELL_KNOWN with RTD_TEXT
	public static NdefMessage getNdefMsg_from_RTD_TEXT(String dataStr,boolean encodeIntUtf8, boolean flagAddAAR) {

		// Locale mylocale=new Locale("en","US");
		// Locale mylocale=new Locale("zh-tw","TW");
		Locale mylocale = new Locale("zh", "TW");
		// 取得預設的編碼格式
		byte[] langBytes = mylocale.getLanguage().getBytes(
				Charset.forName("US-ASCII"));
		// 準備轉換成UTF-8的編碼
		Charset utfEncoding = encodeIntUtf8 ? Charset.forName("UTF-8")
				: Charset.forName("UTF-16");
		// 往下做字元轉換的位移
		int utfBit = encodeIntUtf8 ? 0 : (1 << 7);
		char status = (char) (utfBit + langBytes.length);

		byte[] textBytes = dataStr.getBytes(utfEncoding);
		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte) status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length,textBytes.length);
		NdefRecord textRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_TEXT, new byte[0], data);
		if (flagAddAAR) {
			return new NdefMessage(new NdefRecord[] { textRecord,
					NdefRecord.createApplicationRecord("edu.ntust.cs.rfid.hw2") });
		} else {
			return new NdefMessage(new NdefRecord[] { textRecord });
		}
	}

	boolean writeTag(NdefMessage message, Tag tag) {
		int size = message.toByteArray().length;
		try {
			Ndef ndef = Ndef.get(tag);
			if (ndef != null) {
				ndef.connect();
				try {
					if (!ndef.isWritable()) {
						writeResult = "Tag is read-only.";
						return false;
					} else if (ndef.getMaxSize() < size) {
						writeResult = "The data cannot written to tag,Tag capacity is "
								+ ndef.getMaxSize()
								+ " bytes, message is "
								+ size + " bytes.";
						// ndef.writeNdefMessage(message);
						return false;
					} else {
						ndef.writeNdefMessage(message);
						System.out.println("message=" + message);
						writeResult = "Writte tag successful" + "\n\n";
						tag_scan = false;
						return true;
					}

				} catch (IOException e) {
					writeResult = "Tag refused to connect.";
				} finally {
					ndef.close();
				}

			} else {
				NdefFormatable format = NdefFormatable.get(tag);
				if (format != null) {
					try {
						format.connect();
						try {
							format.format(message);
							writeResult = "The data is written to the tag ";
							return true;
						} catch (IOException e) {
							writeResult = "Failed to format tag.";
						}
					} catch (IOException e) {
						writeResult = "Failed to connect tag.";
						return false;
					} finally {
						format.close();
					}
				} else {
					writeResult = "Tag doesn't support NDEF.";
					return false;
				}
			}
		} catch (Exception e) {
			writeResult = "Failed to write tag";
		}
		return false;
	}

	public String getKeyFromSqlite() {
		String mykey = "";
		try {
			Cursor cursorKey = mDbHelper.getKey_data();
			while (cursorKey.moveToNext()) {
				mykey = cursorKey.getString(0);
			}
			cursorKey.close();
			System.out.println("sqlite Key:" + mykey);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return mykey;
	}

	private void insertKeyToSqlite(String key) {
		mDbHelper.beginTransaction();
		try {
			System.out.println("=====================key=========" + key);
			mDbHelper.deleteKey_data();
			mDbHelper.insertKey_data(key);
			Toast.makeText(NFCMain.this, "key have been saved",	Toast.LENGTH_LONG).show();
			mDbHelper.transactionSuccessful();
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			mDbHelper.endTransaction();
		}
	}
}
